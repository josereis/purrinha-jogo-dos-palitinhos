package purrinha.client;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import purrinha.app.utils.ThreadJogador;

public class Cliente {
	private int porta;
	private String host;
	
	@SuppressWarnings("resource")
	public void run() throws UnknownHostException, IOException {
		Socket cliente = new Socket(this.host, this.porta);
		System.out.println("Cliente se conectou ao servidor!");
		
		PrintStream saida = new PrintStream(cliente.getOutputStream());
		Scanner teclado = new Scanner(System.in);
		
		System.out.println("Digite seu nome > "); saida.println(teclado.nextLine());
		
		ThreadJogador tj = new ThreadJogador(cliente.getInputStream());
		tj.run();
	}
	
	public Cliente(int porta, String host) {
		this.host = host;
		this.porta = porta;
	}
}
