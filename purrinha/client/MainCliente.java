package purrinha.client;

import java.io.IOException;
import java.net.UnknownHostException;

public class MainCliente {
	 public static void main(String[] args) throws UnknownHostException, IOException {
		new Cliente(13051, "127.0.0.1").run();
	}
}
