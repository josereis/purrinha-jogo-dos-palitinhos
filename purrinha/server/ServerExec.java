package purrinha.server;

import java.io.IOException;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class ServerExec {
	
	public static void main(String[] args) {
		JLabel jlabel = new JLabel("Porta de Escuta do Servidor?");
		JTextField txtPorta = new JTextField("12345");
		
		Object [] text = {jlabel, txtPorta};
		JOptionPane.showMessageDialog(null, text);
		
		try {
			new Server(Integer.parseInt(txtPorta.getText())).run();
		} catch (NumberFormatException | IOException e) {
			e.printStackTrace();
		}
	}
}
