package purrinha.server;

import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

import purrinha.app.dominio.*;
import purrinha.app.negociacao.*;
import purrinha.app.utils.ThreadSala;

public class Server {
	private String nome;
	private int id, porta;
	private ArrayList<Sala> salas;
	
	private void criarSala(){
		Sala sala = new Sala();
		ThreadSala threadSala = new ThreadSala(sala);
		
		salas.add(sala);
		threadSala.start();
	}
	
	@SuppressWarnings({ "resource", "unused" })
	private void inserirJogadorSala(Socket cliente) throws IOException{
		boolean encontrou = false;
		
		for(Sala sala: salas) {
			if(!sala.isJogoIniciado()){
				Scanner in = new Scanner(cliente.getInputStream());
				PrintStream out = new PrintStream(cliente.getOutputStream());
				
				// CRIANDO O JOGADOR REFERENTE AO CLIENTE E O INSERIDO NA SALA
				nome = in.nextLine(); Jogador jogador = new Jogador(id++, nome, cliente);
				sala.inserirJogador(jogador);
				
				encontrou = true;
			}
		}
		
		// CASO N�O ENCONTRE UMA SALA DISPONIVEL DEVE CRIAR UMA NOVA SALA E NELA INSERIR O JOGADOR REFERENTE A ESSE CLIENTE
		if(!encontrou) {
			criarSala();
			inserirJogadorSala(cliente);
		}
	}
	
	@SuppressWarnings("unused")
	private void distribuirMensagem(String msg, Sala sala) throws IOException {
		Jogadores listaJogadores = sala.getJogadores();
		
		for(Jogador jogador: listaJogadores.getJogadores()) {
			PrintStream out = new PrintStream(jogador.getCliente().getOutputStream());
			out.println(msg);
		}
	}
	
	@SuppressWarnings("resource")
	public void run() throws IOException {
		ServerSocket server = new ServerSocket(this.porta);
		System.out.println("Porta " + this.porta + " aberta para esculta do servidor!");
		
		while(true) {			
			if(salas.size() > 0) {
				Socket cliente = server.accept();
				inserirJogadorSala(cliente);
			} else {
				criarSala();
				System.out.println("Criada a primeira sala!");
			}	
		}
	}
	
	public Server(int porta) {
		this.id = 0;
		this.porta = porta;
		this.salas = new ArrayList<Sala>();
	}
	
}
