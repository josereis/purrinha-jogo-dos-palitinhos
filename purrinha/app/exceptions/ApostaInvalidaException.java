package purrinha.app.exceptions;

public class ApostaInvalidaException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public ApostaInvalidaException(String msg){
		super(msg);
	}

}
