package purrinha.app.exceptions;

public class JogadorInvalidoException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	
	public JogadorInvalidoException(String msg){
		super(msg);
	}
}
