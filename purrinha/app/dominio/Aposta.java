package purrinha.app.dominio;

public class Aposta {
	private int id, quantidadePalitos; // ID ('id') DO JOGADOR E QUANTIDADE DE PALITOS APOSTADOS PELO JOGADOR
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQuantidadePalitos() {
		return quantidadePalitos;
	}

	public void setQuantidadePalitos(int quantidadePalitos) {
		this.quantidadePalitos = quantidadePalitos;
	}
	
	public Aposta(int id, int quantidadePalitos){
		this.id = id;
		this.quantidadePalitos = quantidadePalitos;
	}
	
}
