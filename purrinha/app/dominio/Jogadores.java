package purrinha.app.dominio;

import java.util.ArrayList;

import purrinha.app.exceptions.*;

public class Jogadores {
	private ArrayList<Jogador> jogadores;
	
	public ArrayList<Jogador> getJogadores() {
		return jogadores;
	}

	public void setJogadores(ArrayList<Jogador> jogadores) {
		this.jogadores = jogadores;
	}
	
	public void inserirJogador(Jogador jogador) {
		if(buscarJogador(jogador.getId()) == null){
			jogadores.add(jogador);
		} else throw new JogadorInvalidoException("Jogador j� adicionado a lista de jogadores");
	}
	
	public void removerJogador(int id) {
		Jogador jogador = buscarJogador(id);
		
		if(jogador != null) {
			jogadores.remove(jogador);
		} else throw new JogadorInvalidoException("Jogador n�o pertence a lista de jogadores");
	}
	
	public Jogador	buscarJogador(int id) {
		for(Jogador jogador: jogadores){
			if(jogador.getId() == id) {
				return jogador;
			}
		}
		
		return null;
	}
	
	public int size(){
		int tam = jogadores.size();
		
		return tam;	
	}
	
	public Jogadores(){
		this.jogadores = new ArrayList<Jogador>();
	}
}
