package purrinha.app.dominio;

public class Resposta {
	private int status;
	private String nome;
	private Aposta aposta;
	private boolean aguardar;
	
	// CONSTANTES USADAS PARA AJUDAR NA IMPLEMENTAÇÃO DO TRABALHO
	public static final int AGUARDANDO_INICIAR = 1, AGUARDANDO_JOGADA = 2, TERMINOU_JOGADA = 3, FIM_JOGO = 4;
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Aposta getAposta() {
		return aposta;
	}

	public void setAposta(Aposta aposta) {
		this.aposta = aposta;
	}

	public boolean isAguardar() {
		return aguardar;
	}

	public void setAguardar(boolean aguardar) {
		this.aguardar = aguardar;
	}
	
	public Resposta(int status, boolean aguardar){
		this.status = status;
		this.aguardar = aguardar;
	}
	
}
