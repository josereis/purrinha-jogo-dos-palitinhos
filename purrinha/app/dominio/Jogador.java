package purrinha.app.dominio;

import java.net.Socket;

public class Jogador {
	private String nome;
	private Socket cliente;
	private int id, quantidadePalitos;
	private boolean estaJogando, colocouPalitosMesa, apostou;
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Socket getCliente() {
		return cliente;
	}
	
	public void setCliente(Socket cliente) {
		this.cliente = cliente;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getQuantidadePalitos() {
		return quantidadePalitos;
	}
	
	public void setQuantidadePalitos(int quantidadePalitos) {
		this.quantidadePalitos = quantidadePalitos;
	}
	
	public boolean getEstaJogando() {
		return estaJogando;
	}
	
	public void setEstaJogando(boolean estaJogando) {
		this.estaJogando = estaJogando;
	}
	
	public boolean getColocouPalitosMesa() {
		return colocouPalitosMesa;
	}
	
	public void setColocouPalitosMesa(boolean colocouPalitosMesa) {
		this.colocouPalitosMesa = colocouPalitosMesa;
	}
	
	public boolean getApostou() {
		return apostou;
	}
	
	public void setApostou(boolean apostou) {
		this.apostou = apostou;
	}
	
	public void decrementarQuantidadePalitos(){
		quantidadePalitos--;
		
		if(quantidadePalitos == 0) {
			setEstaJogando(false);
		}
	}
	
	public Jogador(int id, String nome, Socket cliente){
		this.id = id;
		this.nome = nome;
		this.cliente = cliente;
		this.estaJogando = true;
		this.quantidadePalitos = 3;
	}
	
}
