package purrinha.app.utils;

import java.util.Scanner;
import java.io.InputStream;

public class ThreadJogador extends Thread {
	private InputStream servidor;
	
	public ThreadJogador(InputStream servidor) {
		this.servidor = servidor;
	}
	
	@SuppressWarnings("resource")
	public void run() {
		// RECEBE MENSAGEM DO SERVIDOR E IMPRIME NA TELA
		Scanner saida = new Scanner(this.servidor);
		
		while(saida.hasNextLine()) {
			
			System.out.println(saida.nextLine());
			
		}
	}
}
