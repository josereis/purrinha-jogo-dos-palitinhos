package purrinha.app.utils;

import java.io.IOException;
import java.io.PrintStream;

import purrinha.app.dominio.Jogador;
import purrinha.app.dominio.Jogadores;
import purrinha.app.negociacao.*;

public class ThreadSala extends Thread {
	
	private Sala sala;

	public Sala getSala() {
		return sala;
	}

	public void setSala(Sala sala) {
		this.sala = sala;
	}
	
	private void distribuirMensagem(String msg) throws IOException {
		Jogadores listaJogadores = sala.getJogadores();
		
		for(Jogador jogador: listaJogadores.getJogadores()) {
			PrintStream out = new PrintStream(jogador.getCliente().getOutputStream());
			out.println(msg);
		}
	}
	
	public void run(){
		boolean liberada = false;
		
		try{
			
			while(!liberada){
				int tamanho = sala.getJogadores().size();
				
				String msg;
				if(tamanho < Parametros.MINIMO_JOGADORES){
					msg = "Sala ainda n�o alcan�ou o tamanho m�nimo. " + tamanho + " jogadores em sala.";
					distribuirMensagem(msg);
					
					sleep(7000); // AGUARDA 7 SEGUNDOS
                    continue;
				} else {
					if(tamanho == Parametros.MAXIMO_JOGADORES) {
                        sala.setJogoIniciado(true);
                        msg = ("Jogo come�ou! Sala lotada!"); distribuirMensagem(msg);
                        break;
					}
					
					if(tamanho < Parametros.MAXIMO_JOGADORES) {
						int tempTamanho = sala.getJogadores().getJogadores().size();
						msg = tamanho + " jogadores em sala. Sala alcan�ou tamanho m�nimo, aguardando 10s para caso algum jogador queira entrar."; distribuirMensagem(msg);
						
						sleep(10000); // AGUARDA 10 SEGUNDOS
						
						// CASO N�O TENHA ENTRADO MAIS NENHUM JOGADOR O JOGO � INICIADO
						if(tempTamanho == sala.getJogadores().getJogadores().size()) {
							sala.setJogoIniciado(true);
							msg = "Jogo come�ou!"; distribuirMensagem(msg);
                            break;
						}
					}
				}
				
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public ThreadSala(Sala sala){
		this.setSala(sala);
	}
}
