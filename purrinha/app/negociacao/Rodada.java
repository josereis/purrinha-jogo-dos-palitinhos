package purrinha.app.negociacao;

import java.util.ArrayList;

import purrinha.app.dominio.*;
import purrinha.app.exceptions.*;

public class Rodada implements RodadaInterface {


	private int somaPalitos = 0;
	private boolean estaAtiva = true;
	private ArrayList<Aposta> apostas = new ArrayList<Aposta>();
	
	public int getSomaPalitos() {
		return somaPalitos;
	}

	public void setSomaPalitos(int somaPalitos) {
		this.somaPalitos = somaPalitos;
	}

	public boolean getEstaAtiva() {
		return estaAtiva;
	}

	public void setEstaAtiva(boolean estaAtiva) {
		this.estaAtiva = estaAtiva;
	}

	public ArrayList<Aposta> getApostas() {
		return apostas;
	}

	public void setApostas(ArrayList<Aposta> apostas) {
		this.apostas = apostas;
	}

	@Override
	public int divulgarResultado() {
		for(Aposta aposta: apostas){
			if(aposta.getQuantidadePalitos() == somaPalitos) {
				System.out.println("Jogador " + aposta.getId() + " ganhou");
				
				return aposta.getId();
			}
		}
		
		return -1;
	}

	@Override
	public void realizarJogada(Aposta aposta) {
		// VERIFICAR SE ALGUMA APOSTA JA FOI EFETUADA COM A QUANTIDADE DA APOSTA QUE SE DESEJA FAZER
		for(Aposta ap: apostas) {
			if(ap.getQuantidadePalitos() == aposta.getQuantidadePalitos()){
				// EXCEPTION, POIS A APOSTA COM ESTA QUANTIDADE DE PALITOS JA FOI EFETUADA
				throw new ApostaInvalidaException("Aposta j� realizada, por favor insira outra aposta");
			}
		}
		
		apostas.add(aposta);
		this.somaPalitos += aposta.getQuantidadePalitos();
	}
}
