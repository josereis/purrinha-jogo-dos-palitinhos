package purrinha.app.negociacao;

import purrinha.app.dominio.*;
import purrinha.app.exceptions.*;
import purrinha.app.utils.*;

public class Sala implements SalaInterface {
	
	private Aposta aposta;
	private Jogadores jogadores;
	private ThreadSala salaThread;
	private RodadaInterface rodada;
	private boolean jogoIniciado, jogoTerminou;
	private int proximoJogador = 1, rodadaAtual;
	
	public Aposta getAposta() {
		return aposta;
	}

	public void setAposta(Aposta aposta) {
		this.aposta = aposta;
	}

	public Jogadores getJogadores() {
		return jogadores;
	}

	public void setJogadores(Jogadores jogadores) {
		this.jogadores = jogadores;
	}

	public ThreadSala getSalaThread() {
		return salaThread;
	}

	public void setSalaThread(ThreadSala salaThread) {
		this.salaThread = salaThread;
	}

	public RodadaInterface getRodada() {
		return rodada;
	}

	public void setRodada(RodadaInterface rodada) {
		this.rodada = rodada;
	}

	public boolean isJogoIniciado() {
		return jogoIniciado;
	}

	public void setJogoIniciado(boolean jogoIniciado) {
		this.jogoIniciado = jogoIniciado;
	}

	public boolean isJogoTerminou() {
		return jogoTerminou;
	}

	public void setJogoTerminou(boolean jogoTerminou) {
		this.jogoTerminou = jogoTerminou;
	}

	public int getProximoJogador() {
		return proximoJogador;
	}

	public void setProximoJogador(int proximoJogador) {
		this.proximoJogador = proximoJogador;
	}

	public int getRodadaAtual() {
		return rodadaAtual;
	}

	public void setRodadaAtual(int rodadaAtual) {
		this.rodadaAtual = rodadaAtual;
	}
	
	private void verificarGanhador(int idJogador) {
        if(idJogador != -1) {
            jogadores.buscarJogador(idJogador).decrementarQuantidadePalitos();
        }
    }
	
	public Jogador proximoJogador(boolean incrementar) {
		Jogador jogador = jogadores.buscarJogador(proximoJogador);
		
		if(incrementar) {
			do{
				if(proximoJogador >= jogadores.getJogadores().get(jogadores.getJogadores().size() - 1).getId()){
					proximoJogador = 1;
					
					verificarGanhador(rodada.divulgarResultado());
					
					rodada = new Rodada(); rodadaAtual++;
				} else {
					proximoJogador++;
				}
			}while(!jogadores.buscarJogador(proximoJogador).getEstaJogando());
		}
		
		return jogador;
	}
	
	@Override
	public Jogador proximoJogador() {
		return proximoJogador(true);
	}

	private void jogoTerminou(){
		int jogadoresJogando = 0;
		
		for(Jogador jogador: jogadores.getJogadores()){
			if(jogador.getEstaJogando()){
				jogadoresJogando++;
			}
		}
		
		if(jogadoresJogando <= 1)
			this.jogoTerminou = true;
	}
	
	@Override
	public Resposta esperar(int id) {
		if(!jogoIniciado)
			return new Resposta(Resposta.AGUARDANDO_INICIAR, true);
		
		jogoTerminou(); // FAZ A VERIFICA��O SE O JOGO JA FOI ENCERADO, SETANDO TRUE EM 'jogoTermiou'
		
		if(jogoTerminou) {
			return new Resposta(Resposta.FIM_JOGO, false);
		}
		
		if(!jogadores.buscarJogador(id).getEstaJogando()){
			return new Resposta(Resposta.TERMINOU_JOGADA, false);
		}
		
		if(id == proximoJogador(false).getId()) {
			return new Resposta(Resposta.AGUARDANDO_JOGADA, false);
		} else {
			return new Resposta(Resposta.AGUARDANDO_JOGADA, true);
		}
	}

	@Override
	public void inserirJogador(Jogador jogador) {
		if(jogoIniciado)
			throw new JogadorInvalidoException("O jogo ja foi iniciado!");
		
		jogadores.inserirJogador(jogador);
		if(!jogoIniciado){ //  && !salaThread.isAlive()
			System.out.printf("Jogador: " + jogador.getNome() + " inserido com sucesso!!\n");
			//salaThread.start();
		}
	}
	
	private String pegarEstatisticas() {
        String retorno = "\nPalitos: ";

        for(Jogador jogador : jogadores.getJogadores()) {
            retorno += jogador.getNome() + " (" + jogador.getQuantidadePalitos() + "), ";
        }

        retorno = retorno.substring(0, retorno.length() - 2);
        return retorno;
    }

	@Override
	public String divulgarResultado(int idJogador) {
		StringBuilder retorno = new StringBuilder();
		
		Jogador jogador = jogadores.buscarJogador(idJogador), ultimoJogador = null;
		if(aposta != null){
			ultimoJogador = jogadores.buscarJogador(aposta.getId());
		}
		
		retorno.append("\n==================\n")
        .append("Rodada " + rodadaAtual + "\n")
        .append("Tenho " + jogador.getQuantidadePalitos() + " palitos.\n");
		
		if(ultimoJogador != null) {
			retorno.append("�ltimo jogador foi: " + ultimoJogador.getNome() + ", ele apostou " + aposta.getQuantidadePalitos() + "\n");
		}
		
		retorno.append("Jogador jogando: " + jogadores.buscarJogador(proximoJogador).getNome());
        retorno.append(pegarEstatisticas());
        retorno.append("\n==================");
		
		return retorno.toString();
	}
	
	private void verificarPalitosJogados(int id, int quantidadePalitosJogados){
		Jogador jogador = jogadores.buscarJogador(id);
		
		if((rodadaAtual == 1) && (quantidadePalitosJogados == 0))
			throw new ApostaInvalidaException("N�o se pode jogar 0 palitos na primeira rodada!");
		
		if(jogador.getQuantidadePalitos() < quantidadePalitosJogados)
			throw new ApostaInvalidaException("Voc� jogou " + quantidadePalitosJogados + " palitos, uma quantidade maior do que possuida. Voc� possui " + jogador.getQuantidadePalitos() + "palitos disponiveis.");
	}

	@Override
	public void realizarJogada(int quantidadePalitos, Aposta aposta) {
		verificarPalitosJogados(aposta.getId(), quantidadePalitos); // VERIFICA SE A QUANTIDADE DE PALITOS JOGADAS � VALIDA
		
		// CAJO A APOSTA SEJA VALIDA, A JOGADA � EFETUADA
		rodada.realizarJogada(aposta);
		this.aposta = aposta;
		
		proximoJogador();
	}
	
	public void iniciarJogo(){
		
	}
	
	public Sala(){
		super();
		this.rodada = new Rodada();
		this.jogadores = new Jogadores();
		//this.salaThread = new ThreadSala(this);
		
		this.rodadaAtual = 1;
		this.jogoTerminou = false;
	}
}
