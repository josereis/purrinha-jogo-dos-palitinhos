package purrinha.app.negociacao;

import purrinha.app.dominio.Aposta;

public interface RodadaInterface {
	
	public int divulgarResultado();
	public void realizarJogada(Aposta aposta);

}
