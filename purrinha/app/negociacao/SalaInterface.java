package purrinha.app.negociacao;

import purrinha.app.dominio.*;

public interface SalaInterface {
	
	public Jogador proximoJogador();
	public Resposta esperar(int id);
	public void inserirJogador(Jogador jogador);
	public String divulgarResultado(int idJogador);
	public void realizarJogada(int quantidadePalitos, Aposta aposta);
}
